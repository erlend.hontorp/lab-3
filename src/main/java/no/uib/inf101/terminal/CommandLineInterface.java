package no.uib.inf101.terminal;

// UiB INF101 ShellLab - CommandLineInterface.java
//
// Grensesnittet CommanLineInterface beskriver metoder for input og
// output til et program som virker med et tekstbasert grensesnitt.

public interface CommandLineInterface {
  void keyPressed(char key);
  String getScreenContent();
}
