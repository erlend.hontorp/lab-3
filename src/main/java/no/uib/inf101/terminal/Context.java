package no.uib.inf101.terminal;

// UiB INF101 ShellLab - Context.java
//
// Denne filen inneholder kode som navigerer et filsystem.

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class Context {

  private final File home;
  private File cwd;

  public Context() {
    this(new File(System.getProperty("user.dir")));
  }

  public Context(File home) {
    try {
      this.home = home.getCanonicalFile();
      this.cwd = this.home;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public boolean goToPath(String path) {
    File newDir = new File(this.cwd, path);
    if (!newDir.isDirectory()) {
      return false;
    }
    try {
      this.cwd = newDir.getCanonicalFile();
      return true;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public File getCwd() {
    return this.cwd;
  }

  public void goToHome() {
    this.cwd = this.home;
  }

  public boolean isAtHome() {
    return Objects.equals(this.cwd, this.home);
  }
}
